import { z } from "zod";
import { Timestamp, TimestampSchema } from "./timestamp";

export const DataPointSchema = z.object({
  timestamp: TimestampSchema,
  value: z.number(),
});

export type DataPointPOD = z.infer<typeof DataPointSchema>;

//Neagitve values will be set to zero by default
export class DataPoint implements DataPointPOD {
  timestamp: Timestamp;
  value: number;

  constructor(data: DataPointPOD) {
    let {value, timestamp} = DataPointSchema.parse(data);
    this.timestamp = new Timestamp(timestamp);
    this.value = value;
    if (this.value < 0) {
      this.value = 0;
    }
  }

  clone = () => {
    return new DataPoint({
      value: this.value,
      timestamp: this.timestamp.clone(),
    });
  };
}
