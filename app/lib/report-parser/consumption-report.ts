import { Timestamp } from "./timestamp";
import { DataPoint } from "./datapoint";

/**
 * Contains value points for every hour of the year
 */
class ConsumptionReport {
  public dataPoints: Array<DataPoint> = [];

  //Help list: Flag here all added timestamps as true. Indexes as [month][day][hour]
  private timestampIndex: Array<Array<Array<boolean>>> = [[[]]];

  private timestampIsAdded(timestamp: Timestamp) {
    const { month, day, hour } = timestamp;
    return !!this.timestampIndex[month]?.[day]?.[hour];
  }

  private addTimestamp(timestamp: Timestamp) {
    const { month, day, hour } = timestamp;

    if (!Array.isArray(this.timestampIndex[month])) {
      this.timestampIndex[month] = [];
    }

    if (!Array.isArray(this.timestampIndex[month][day])) {
      this.timestampIndex[month][day] = [];
    }

    this.timestampIndex[month][day][hour] = true;
  }

  // Will return an array with 2 elements
  // 0 index is copy of all the points that fullfill the condition
  // 1 index is copy of all the points that do not fullfill the condition
  public splitPointsByCondition(
    condition: (point: DataPoint) => boolean
  ): [Array<DataPoint>, Array<DataPoint>] {
    let validPoints: Array<DataPoint> = [];
    let failedPoints: Array<DataPoint> = [];

    this.dataPoints.forEach((point) => {
      condition(point)
        ? validPoints.push(point.clone())
        : failedPoints.push(point.clone());
    });

    return [validPoints, failedPoints];
  }

  public addDataPoint(dataPoint: DataPoint) {
    //Check if timestamp already added
    if (this.timestampIsAdded(dataPoint.timestamp)) {
      //Sum to existing timestamp
      let existingPoint = this.dataPoints.find((p) =>
        p.timestamp.isSameAs(dataPoint.timestamp)
      );
      if (existingPoint !== undefined) {
        existingPoint.value += dataPoint.value;
        return;
      }
    }

    //Flag timestamp as added
    this.addTimestamp(dataPoint.timestamp);

    this.dataPoints.push(dataPoint.clone());
  }

  //Will return list of all hours of the year that are missing
  public getMissingTimestamps() {
    let missingTimes: Array<Timestamp> = [];

    //Check all dates (ignore 29th feb since no info about the year)
    const monthDayCounts = [0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

    for (let month = 1; month <= 12; month++) {
      let dayCount = monthDayCounts[month];
      for (let day = 1; day <= dayCount; day++) {
        for (let hour = 0; hour <= 23; hour++) {
          //Expect misisng timestamp when moving from winter time to summer time
          if (hour === 3 && month === 3) continue;
          let timestamp = new Timestamp({ month, day, hour });
          if (!this.timestampIsAdded(timestamp)) {
            missingTimes.push(timestamp);
          }
        }
      }
    }

    return missingTimes;
  }
}

export { ConsumptionReport };
