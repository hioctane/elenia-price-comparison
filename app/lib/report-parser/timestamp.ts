import { z } from "zod";

export const TimestampSchema = z.object({
  month: z.number().min(1).max(12),
  day: z.number().min(1).max(31),
  hour: z.number().min(0).max(23),
});

export type TimestampPOD = z.infer<typeof TimestampSchema>;

export class Timestamp implements TimestampPOD {
  month: number;
  day: number;
  hour: number;

  constructor(data: TimestampPOD) {
    const { month, day, hour } = TimestampSchema.parse(data);
    this.month = month;
    this.day = day;
    this.hour = hour;
  }

  clone = () => {
    let {month, day, hour} = this; 
    return new Timestamp({month, day, hour});
  }

  compare = (t: Timestamp) => {
    if (this.month !== t.month) {
      return this.month - t.month;
    }

    if (this.day !== t.day) {
      return this.day - t.day;
    }

    return this.hour - t.hour;
  };

  isBetween = (start: Timestamp, end: Timestamp) => {
    let startBeforeEnd = start.isBeforeOrSameAs(end);
    if (startBeforeEnd) {
      return this.isAfterOrSameAs(start) && this.isBeforeOrSameAs(end);
    }

    //Start is "after" end. For example: Dec 1st - Mar 28th
    return this.isAfterOrSameAs(start) || this.isBeforeOrSameAs(end);
  };

  hourIsBetween = (start: number, end: number) => {
    let startBeforeEnd = start <= end;
    if (startBeforeEnd) {
      return this.hour >= start && this.hour <= end;
    }

    //Start is "after" end. For example: 22:00 - 06:00
    return this.hour >= start || this.hour <= end;
  }

  isSameAs = (t: Timestamp) => {
    return this.compare(t) === 0;
  };

  isAfterOrSameAs = (t: Timestamp) => {
    return this.compare(t) >= 0;
  };

  isBeforeOrSameAs = (t: Timestamp) => {
    return this.compare(t) <= 0;
  }
}
