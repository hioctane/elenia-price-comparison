import { Timestamp } from "./timestamp";

let jan1st_10 = new Timestamp({ month: 1, day: 1, hour: 10 });
let feb10th_20 = new Timestamp({ month: 2, day: 10, hour: 20 });
let dec24th_00 = new Timestamp({ month: 12, day: 24, hour: 0 });

describe("Timestamp", () => {
  it("isBeforeAs returns correct results", () => {
    expect(jan1st_10.isBeforeOrSameAs(feb10th_20)).toBe(true);
    expect(feb10th_20.isBeforeOrSameAs(dec24th_00)).toBe(true);
    expect(dec24th_00.isBeforeOrSameAs(feb10th_20)).toBe(false);
    expect(jan1st_10.isAfterOrSameAs(jan1st_10)).toBe(true);
  });

  it("isAfterAs returns correct results", () => {
    expect(jan1st_10.isAfterOrSameAs(feb10th_20)).toBe(false);
    expect(feb10th_20.isAfterOrSameAs(dec24th_00)).toBe(false);
    expect(dec24th_00.isAfterOrSameAs(feb10th_20)).toBe(true);
    expect(jan1st_10.isAfterOrSameAs(jan1st_10)).toBe(true);
  });

  it("isBetween returns correct results", () => {
    expect(feb10th_20.isBetween(jan1st_10, dec24th_00)).toBe(true);
    expect(feb10th_20.isBetween(dec24th_00, jan1st_10)).toBe(false);
    expect(jan1st_10.isBetween(dec24th_00, feb10th_20)).toBe(true);
    expect(jan1st_10.isBetween(jan1st_10, jan1st_10)).toBe(true);
  });

  it("isSameAs returns correct results",  () => {
    expect(jan1st_10.isSameAs(jan1st_10)).toBe(true);
    expect(jan1st_10.isSameAs(feb10th_20)).toBe(false);
  });
});

export {};
