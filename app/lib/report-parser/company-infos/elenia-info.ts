import { ConsumptionReport } from "../consumption-report";
import { CompanyInfo } from "../report-parser";
import { Timestamp } from "../timestamp";
import { DataPoint } from "../datapoint";

export const EleniaInfo: CompanyInfo = {
  id: "elenia",
  name: "Elenia",
  plans: [
    {
      name: "Yleissiirto",
      type: "basic",
      basicFee: 21.24,
      kwhFee: 0.0841,
    },
    {
      name: "Yösiirto",
      type: "night",
      basicFee: 36.9,
      dayFee: 0.0712,
      nightFee: 0.0543,
      dayFirstHour: 7,
      dayLastHour: 22,
    },
    {
      name: "Vuodenaikasiirto",
      type: "season",
      basicFee: 43.9,
      winterDayFee: 0.0810,
      nonWinterDayFee: 0.0543,
      dayFirstHour: 7,
      dayLastHour: 22,
      winterStart: new Timestamp({ month: 11, day: 1, hour: 0 }),
      winterEnd: new Timestamp({ month: 3, day: 31, hour: 23 }),
    },
  ],
  csvParser: (input: string): ConsumptionReport => {
    let report = new ConsumptionReport();
    let rows = input.split("\n");

    rows.splice(0, 1); //remove first row
    rows = rows.filter((r) => r.length > 5); //remove empty or very short rows
    rows.splice(rows.length - 1); //remove last row

    //Parse consumption values from CSV rows
    for (let r of rows) {
      let [dateAndTime, valueStr] = r.split(";");
      let [dateStr, timeStr] = dateAndTime.split(" ");
      let [day, month] = dateStr.split(".").map((v) => parseInt(v));

      let timestamp = new Timestamp({
        day,
        month,
        hour: parseInt(timeStr.split(":")[0].toString()),
      });

      let value = parseFloat(valueStr.replace(",", "."));

      let datapoint = new DataPoint({ value, timestamp });

      report.addDataPoint(datapoint);
    }

    return report;
  },
};
