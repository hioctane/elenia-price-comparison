import { ConsumptionReport } from "./consumption-report";
import { DataPoint } from "./datapoint";

function csvRowToDatapoint(row: string): DataPoint {
  let splitted = row.split(";");
  let date = new Date(splitted[5]);
  let value = parseFloat(splitted[6].replace(",", "."));

  let timestamp = {
    month: date.getMonth() + 1,
    day: date.getDate(),
    hour: date.getHours(),
  };

  return new DataPoint({
    timestamp: timestamp,
    value: value,
  });

}

export function parseFingridCsv(input: string) {
  let report = new ConsumptionReport();

  let allRows = input.split("\n");

  //Remove first and last row
  let dataRows = allRows.splice(1, allRows.length - 2);
  let dataPoints = dataRows.map(csvRowToDatapoint);

  dataPoints.forEach((dp) => report.addDataPoint(dp));

  return report;
}
