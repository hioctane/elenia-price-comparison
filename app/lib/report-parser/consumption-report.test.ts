import { ConsumptionReport } from "./consumption-report";
import { DataPoint } from "./datapoint";
import { Timestamp } from "./timestamp";

describe("consumption-report", () => {
  it("empty report will find 365 * 24 missing timestamps", () => {
    const report = new ConsumptionReport();
    const missing = report.getMissingTimestamps();
    expect(missing.length).toBe(365 * 24);
  });

  it("sums values when adding existing timestamp", () => {
    const report = new ConsumptionReport();
    const dataPoint1: DataPoint = new DataPoint({
      value: 123,
      timestamp: new Timestamp({
        month: 1,
        day: 1,
        hour: 1,
      }),
    });

    const dataPoint2: DataPoint = new DataPoint({
      value: 456,
      timestamp: new Timestamp({
        month: 1,
        day: 2,
        hour: 1,
      }),
    });

    report.addDataPoint(dataPoint1);
    report.addDataPoint(dataPoint2);

    //Will add value to the first datapoin, because of the same timestamp
    report.addDataPoint(dataPoint1); 

    expect(report.dataPoints[0].value).toBe(123 * 2);
  });

  it("contains valid data with 0 missing", () => {
    const report = new ConsumptionReport();

    //Since no validation for individual month lengths
    //Use 31 for all months
    for (let month = 1; month <= 12; month++) {
      for (let day = 1; day <= 31; day++) {
        for (let hour = 0; hour <= 23; hour++) {
          const dataPoint: DataPoint = new DataPoint({
            value: 1,
            timestamp: new Timestamp({
              month,
              day,
              hour,
            }),
          });
          report.addDataPoint(dataPoint);
        }
      }
    }

    let sum = report.dataPoints.reduce((cur, acc) => {
      return acc.value + cur;
    }, 0);
    expect(sum).toBe(12 * 31 * 24);

    let missing = report.getMissingTimestamps();
    expect(missing.length).toBe(0);
  });
});

export {};
