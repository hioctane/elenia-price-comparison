import { z } from "zod";
import { ConsumptionReport } from "../consumption-report";
import { PlanReport } from "./plans";
import { Timestamp } from "../timestamp";
import { DataPoint } from "../datapoint";

export const SeasonPlanDefinitionSchema = z.object({
  name: z.string(),
  type: z.string().regex(new RegExp("season")),
  basicFee: z.number().nonnegative(),
  nonWinterDayFee: z.number().nonnegative(),
  winterDayFee: z.number().nonnegative(),
  dayFirstHour: z.number().nonnegative(),
  dayLastHour: z.number().nonnegative(),
  winterStart: z.instanceof(Timestamp),
  winterEnd: z.instanceof(Timestamp),
});

export type SeasonPlanDefinition = z.infer<typeof SeasonPlanDefinitionSchema>;

export function getSeasonPlanReport(
  definition: SeasonPlanDefinition,
  consumptionReport: ConsumptionReport
): PlanReport {
  const {
    winterStart,
    winterEnd,
    basicFee,
    winterDayFee,
    nonWinterDayFee,
    dayFirstHour,
    dayLastHour,
  } = definition;

  const isWinterDay = (point: DataPoint) => {
    return (
      point.timestamp.isBetween(winterStart, winterEnd) &&
      point.timestamp.hourIsBetween(dayFirstHour, dayLastHour)
    );
  };

  const [winterDayPoints, nonWinterDayPoints] =
    consumptionReport.splitPointsByCondition(isWinterDay);

  const winterDayConsumption = winterDayPoints.reduce(
    (acc, cur) => acc + cur.value,
    0
  );

  const nonWinterDayConsumption = nonWinterDayPoints.reduce(
    (acc, cur) => acc + cur.value,
    0
  );

  const basicTotal = basicFee * 12;
  const winterDayFeeTotal = winterDayConsumption * winterDayFee;
  const nonWinterDayFeeTotal = nonWinterDayConsumption * nonWinterDayFee;

  return {
    name: definition.name,
    rows: [
      {
        description: `Perusmaksu: ${basicFee.toFixed(2)} x 12kk`,
        value: basicTotal,
      },
      {
        description: `Talvipäivien kulutus: ${winterDayFee.toFixed(
          2
        )} x ${winterDayConsumption.toFixed(2)}kWh`,
        value: winterDayFeeTotal,
        points: winterDayPoints,
      },
      {
        description: `Muu kulutus: ${nonWinterDayFee.toFixed(
          2
        )} x ${nonWinterDayConsumption.toFixed(2)}kWh`,
        value: nonWinterDayFeeTotal,
        points: nonWinterDayPoints,
      },
    ],
  };
}
