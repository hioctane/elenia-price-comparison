import { z } from "zod";
import { ConsumptionReport } from "../consumption-report";
import { PlanReport } from "./plans";

export const BasicPlanDefinitionSchema = z.object({
  name: z.string(),
  type: z.literal("basic"),
  basicFee: z.number().nonnegative(),
  kwhFee: z.number().nonnegative(),
});

export type BasicPlanDefinition = z.infer<typeof BasicPlanDefinitionSchema>;

export function getBasicPlanReport(
  definition: BasicPlanDefinition,
  consumptionReport: ConsumptionReport
): PlanReport {
  const { name, basicFee, kwhFee } = definition;
  const totalConsumption = consumptionReport.dataPoints.reduce(
    (acc, cur) => acc + cur.value,
    0
  );

  let basicTotal = basicFee * 12;
  let kwhTotal = kwhFee * totalConsumption;

  return {
    name: name,
    rows: [
      {
        description: `Perusmaksu: ${basicFee.toFixed(2)} x 12kk`,
        value: basicTotal,
      },
      {
        description: `Kulutusmaksu: ${kwhFee} x ${totalConsumption.toFixed(2)} x kWh`,
        value: kwhTotal,
      },
    ],
  };
}
