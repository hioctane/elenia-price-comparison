import { DataPoint } from "../datapoint";
import { ConsumptionReport } from "./../consumption-report";
import {
  BasicPlanDefinitionSchema,
  BasicPlanDefinition,
  getBasicPlanReport,
} from "./basic-plan";

import {
  NightPlanDefinitionSchema,
  NightPlanDefinition,
  getNightPlanReport,
} from "./night-plan";

import {
  SeasonPlanDefinitionSchema,
  SeasonPlanDefinition,
  getSeasonPlanReport,
} from "./season-plan";

interface PlanReport {
  name: string;
  rows: Array<{
    description: string;
    value: number;
    points?: Array<DataPoint>;
  }>;
}

function getPlanReport(
  definition: PlanDefinition,
  consumptionReport: ConsumptionReport
): PlanReport {
  //Check if is Basic plan
  const basicParseResult = BasicPlanDefinitionSchema.safeParse(definition);
  if (basicParseResult.success) {
    const basic: BasicPlanDefinition = basicParseResult.data;
    return getBasicPlanReport(basic, consumptionReport);
  }

  //Check if is Night plan
  const nightParseResult = NightPlanDefinitionSchema.safeParse(definition);
  if (nightParseResult.success) {
    const night: NightPlanDefinition = nightParseResult.data;
    return getNightPlanReport(night, consumptionReport);
  }

  //Check if is Season plan
  const seasonParseResult = SeasonPlanDefinitionSchema.safeParse(definition);
  if (seasonParseResult.success) {
    const season: SeasonPlanDefinition = seasonParseResult.data;
    return getSeasonPlanReport(season, consumptionReport);
  }

  throw new Error(
    `Trying to get report from invalid plan definition: ${JSON.stringify(
      definition
    )}`
  );
}

type PlanDefinition =
  | BasicPlanDefinition
  | NightPlanDefinition
  | SeasonPlanDefinition;

export type { PlanReport, PlanDefinition };

export { getPlanReport };
