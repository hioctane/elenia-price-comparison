import { z } from "zod";
import { ConsumptionReport } from "../consumption-report";
import { DataPoint } from "../datapoint";
import { PlanReport } from "./plans";

export const NightPlanDefinitionSchema = z.object({
  name: z.string(),
  type: z.literal("night"),
  basicFee: z.number().nonnegative(),
  dayFee: z.number().nonnegative(),
  nightFee: z.number().nonnegative(),
  dayFirstHour: z.number().min(0).max(23),
  dayLastHour: z.number().min(0).max(23),
});

export type NightPlanDefinition = z.infer<typeof NightPlanDefinitionSchema>;

export function getNightPlanReport(
  definition: NightPlanDefinition,
  consumptionReport: ConsumptionReport
): PlanReport {
  const { name, basicFee, nightFee, dayFee, dayFirstHour, dayLastHour } =
    definition;

  const isDay = (point: DataPoint) => {
    return point.timestamp.hourIsBetween(dayFirstHour, dayLastHour);
  };

  const [dayPoints, nightPoints] = consumptionReport.splitPointsByCondition(isDay);

  const totalDayConsumption = dayPoints.reduce(
    (acc, cur) => acc + cur.value,
    0
  );

  const totalNightConsumption = nightPoints.reduce(
    (acc, cur) => acc + cur.value,
    0
  );

  const basicFeeTotal = 12 * definition.basicFee;
  const nightFeeTotal = totalNightConsumption * nightFee;
  const dayFeeTotal = totalDayConsumption * dayFee;

  return {
    name: name,
    rows: [
      {
        description: `Perusmaksu: ${basicFee.toFixed(2)} x 12kk`,
        value: basicFeeTotal,
      },
      {
        description: `Yö-kulutus: ${nightFee} x ${totalNightConsumption.toFixed(
          2
        )}kWh`,
        value: nightFeeTotal,
        points: nightPoints
      },
      {
        description: `Päivä-kulutus: ${dayFee} x ${totalDayConsumption.toFixed(
          2
        )}kWh`,
        value: dayFeeTotal,
        points: dayPoints
      },
    ],
  };
}
