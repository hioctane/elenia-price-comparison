import { EleniaInfo } from "./company-infos/elenia-info";
import { ConsumptionReport } from "./consumption-report";
import { parseFingridCsv } from "./parse-fingrid-csv";
import {
  getPlanReport,
  PlanDefinition,
  PlanReport,
} from "./plans/plans";

export interface CompanyInfo {
  id: string;
  name: string;
  plans: Array<PlanDefinition>;
  csvParser: (input: string) => ConsumptionReport;
}

const allCompanies: Array<CompanyInfo> = [EleniaInfo];

interface ReportParsingResult {
  result?: Array<PlanReport>;
  error?: string;
}

function parseReport(
  companyId: string,
  input: string
): ReportParsingResult {
  try {
    const companyInfo = allCompanies.find(c => c.id === companyId);
    if (companyInfo === undefined) {
      return { error: `invlaid companyId: ${companyId}`}
    }
    //const report = companyInfo.csvParser(input);
    let report = parseFingridCsv(input);
    console.log({missing: report.getMissingTimestamps()})
    const planReports = companyInfo.plans.map((plan) =>
      getPlanReport(plan, report)
    );

    return { result: planReports };
  } catch (e) {
    //TODO get better error message here to help the client
    console.log(e);
    return { error: "error while parsing data" };
  }
}

export type { ReportParsingResult };

export { allCompanies, parseReport };
