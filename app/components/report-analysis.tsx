import { PlanReport } from "../lib/report-parser/plans/plans";
import TotalCard from "./total-card";

interface ReportAnalysisProps {
  planReports: PlanReport[];
}

export default function ReportAnalysis(props: ReportAnalysisProps) {
  const { planReports } = props;
  const cards = planReports.map((p) => (
    <TotalCard key={p.name} title={p.name} rows={p.rows}></TotalCard>
  ));

  return <div>{cards}</div>;
}
