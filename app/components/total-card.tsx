interface TotalCardProps {
  title: string;
  rows: Array<{ description: string; value: number }>;
}

export type { TotalCardProps };
export default function TotalCard(props: TotalCardProps) {
  let totalSum = props.rows.reduce((acc, cur) => (acc + cur.value), 0).toFixed(2);

  let rowElements = props.rows.map((r, i) => (
    <div key={i} style={{display: "flex"}}>
      <div style={{minWidth: "300px"}}>{r.description}</div>
      <div>{r.value.toFixed(2)}€</div>
    </div>
  ));

  return (
    <div>
      <h1>{props.title}</h1>
      {rowElements}
      <div style={{borderTop: "1px solid black"}}></div>
      <div style={{display: "flex"}}>
        <div style={{minWidth: "300px"}}>Yhteensä</div>
        <div>{totalSum}€</div>
      </div>
    </div>
  );
}
