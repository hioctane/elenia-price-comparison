import { useState } from "react";
import { PlanReport } from "../lib/report-parser/plans/plans";
import {
  parseReport,
} from "../lib/report-parser/report-parser";

interface ReportFileSelectorProps {
  onFileSelected: (file: PlanReport[]) => void;
}

interface ConsumptionValue {
  value: number;
  day: number;
  month: number;
  hour: number;
}

interface CsvParseResult {
  missingValues: Array<ConsumptionValue>;
  consumptionValues: Array<ConsumptionValue>;
}

export type { ConsumptionValue, CsvParseResult };
export default function ReportFileSelector(props: ReportFileSelectorProps) {
  const { onFileSelected } = props;
  let [errorMsg, setErrorMsg] = useState("");
  
  const handleFileSelected = (event: any) => {
    let file = event?.target?.files?.[0];
    if (!file) return;

    const reader = new FileReader();
    reader.readAsText(file, "UTF-8");

    reader.onload = (evt) => {
      const csvFileContent = evt?.target?.result;
      if (!csvFileContent) return;

      const parsed = parseReport("elenia", csvFileContent.toString());
      const { result, error } = parsed;
      if (result !== undefined) {

        //Adding one to analytics counter
        //No personal information will be stored
        fetch("http://u.veivo.dy.fi:8088/laskuri/csvadd.php", {method: "POST"});

        onFileSelected(result);
      } else if (error) {
        //TODO Pass error to client
        setErrorMsg(error);
      }
    };
  }

  return (
    <div>
      <h1>Valitse CSV Raportti</h1>
      <p style={{ marginTop: "-20px" }}>
        <a rel="noreferrer" href="https://oma.datahub.fi/" target="_blank">Fingrid data hubista</a> exportattu yhden vuoden tuntikulutusraportti
      </p>
      <input type="file" onChange={handleFileSelected}></input>
      <div style={{color: "red"}}>{errorMsg}</div>
      <p style={{marginTop: "20px", color: "#ffa5a8"}}>HUOM! Tämä sivusto on epävirallinen harrasteprojekti.<br/>Emme toimi yhteistyössä minkään yrityksen kanssa. <br/>Tarkista aina hintatiedot itse, sillä sivustolla voi olla vanhentunutta tietoa!</p>
    </div>
  );
}
