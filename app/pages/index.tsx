import Head from "next/head";
import styles from "../styles/Home.module.css";
import { useState } from "react";
import ReportFileSelector from "../components/report-file-selector";
import ReportAnalysis from "../components/report-analysis";
import { PlanReport } from "../lib/report-parser/plans/plans";

export default function Home() {
  let [parsedData, setParsedData] = useState<null | PlanReport[]>(null);

  let pageContent =
    parsedData === null ? (
      <ReportFileSelector
        onFileSelected={(file: PlanReport[]) => setParsedData(file)}
      />
    ) : (
      <ReportAnalysis planReports={parsedData} />
    );

  return (
    <div className={styles.container}>
      <Head>
        <title>Elenia CSV Report Analyzer</title>
        <meta name="description" content="Generated by create next app" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>{pageContent}</main>
      <p
        style={{
          margin: "0px",
          padding: "0px",
          position: "absolute",
          bottom: "0px",
          right: "10px",
          color: "gray",
          fontSize: "40px",
        }}
      >
        <a
          rel="noreferrer"
          href="http://u.veivo.dy.fi:8088/laskuri"
          target="_blank"
        >
          .
        </a>
      </p>
    </div>
  );
}
